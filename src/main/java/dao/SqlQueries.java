package dao;

public interface SqlQueries {

    String GET_VEHICLES="select v.update_loop updateLoop,v.description description,v.plate_number plateNumber,v.vehicle_key vehicleKey from vehicles v\n" +
            "where v.plate_number=:plateNumber and v.vehicle_key=:vehicleKey";



    String GET_SCHEDULE_ID="select vs.schedule_id scheduleId from vehicles v " +
            "left outer join vehicle_schedule vs on v.id=vs.vehicle_id " +
            "where v.plate_number=:plateNumber and v.vehicle_key=:vehicleKey ";


    String GET_TRACK_LIST="select t.id,t.name name,t.original_name originalName,t.description,t.file_length length, " +
            "t.file_size size,(time_format(concat(st.begin_hour,\":\",st.begin_minute,\":\",st.begin_second),\"%H:%i:%s\")) beginTime, " +
            "(time_format(concat(st.end_hour,\":\",st.end_minute,\":\",st.end_second),\"%H:%i:%s\")) endTime " +
            "from schedule_tracks st,tracks t,schedules s  " +
            "where s.id=st.sch_id and t.id=st.track_id and s.id=:scheduleId";


    String CHECK_VEHICLE="select count(id) from vehicles v " +
            "where v.plate_number=:plateNumber and v.vehicle_key=:vehicleKey";


    String GET_VEHICLE_ID="select id from vehicles " +
            "where plate_number=:plateNumber and vehicle_key=:vehicleKey";

    String GET_UNICAL_TRACK_ID="select distinct t.id" +
            " from schedule_tracks st,tracks t,schedules s " +
            "where s.id=st.sch_id and t.id=st.track_id and s.id=:sId";

    String ADD_VEHICLES_TRACK="insert into vehicle_tracks(vehicle_id,schedul_id,track_id)" +
            " values(:vId,:sId,:tId)";


    String UPDATE_VEHICLES_TRACK="update vehicle_tracks set status=:status where track_id=:tId";

    String ADD_LOG="insert into logs(vehicle_id,lat,lng,api_name,request_body,response_body) " +
            "values(:id,:lat,:lng,:apiName,:requestBody,:responseBody)";


    String DELETE_VEHICLE_TRACK="delete from vehicle_tracks " +
            "where vehicle_id=:vId";

    String ADD_ANDROID_LOG="insert into android_logs(vehicle_id,path) " +
            "values(:vehicleId,:path)";









}
