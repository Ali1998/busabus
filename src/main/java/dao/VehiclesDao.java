package dao;

import java.util.List;
import domain.ResponseTrack;
import domain.ResponseVehicles;
import domain.Track;
import domain.TrackVehicle;


import javax.sql.DataSource;


public interface VehiclesDao {

    void setJdbc(DataSource jdbc);

    ResponseVehicles getScheduleData(String plateNumber, String vehicleKey);

    Integer getScheduleId(String plateNumber, String vehicleKey);

    ResponseTrack getTracks(Integer scheduleId);

    boolean CheckVehicle(String plateNumber, String vehicleKey);

    Integer  getVehicleId(String plateNumber,String vehicleKey);

    List<Track> getTrackIdList(Integer scheduleId);

    boolean addVehicleTrack(TrackVehicle trackVehicle);

    boolean updateVehicleTrack(Integer status,Integer trackId);

    boolean deleteVehicleTrack(Integer vehicleId);












}
