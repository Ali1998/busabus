package dao;

import domain.AndroidLog;
import domain.Log;

import javax.sql.DataSource;

public interface LogDao {



    void setJdbc(DataSource jdbc);

    boolean addLog(Log log);

    boolean addAndroidLog(AndroidLog log);




}
