package dao.impl;


import dao.LogDao;
import dao.SqlQueries;
import domain.AndroidLog;
import domain.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;

@Repository
public class LogDaoImpl  implements LogDao {


    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")
    @Override
    public void setJdbc(DataSource jdbc) {
        this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public boolean addLog(Log log) {
        return jdbcTemplate.update(SqlQueries.ADD_LOG,new BeanPropertySqlParameterSource(log))>0;
    }

    @Override
    public boolean addAndroidLog(AndroidLog log) {
        return jdbcTemplate.update(SqlQueries.ADD_ANDROID_LOG,new BeanPropertySqlParameterSource(log))>0;
    }
}
