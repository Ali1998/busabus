package dao.impl;


import dao.SqlQueries;
import dao.VehiclesDao;
import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.*;


@Repository
public class VehiclesDaoImpl implements VehiclesDao {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("dataSource")

    @Override
    public void setJdbc(DataSource jdbc) {
       this.jdbcTemplate=new NamedParameterJdbcTemplate(jdbc);
    }

    @Override
    public ResponseVehicles getScheduleData(String plateNumber, String vehicleKey) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("vehicleKey",vehicleKey);
        param.addValue("plateNumber",plateNumber);
        return jdbcTemplate.queryForObject(SqlQueries.GET_VEHICLES,param,BeanPropertyRowMapper.newInstance(ResponseVehicles.class));
    }

    @Override
    public Integer getScheduleId(String plateNumber, String vehicleKey) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("plateNumber",plateNumber);
        param.addValue("vehicleKey",vehicleKey);
        return jdbcTemplate.queryForObject(SqlQueries.GET_SCHEDULE_ID,param,Integer.class);
    }

    @Override
    public ResponseTrack getTracks(Integer scheduleId) {

        String url=Config.TRACK_URL;
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("scheduleId",scheduleId);
        List<Track>track;
        track=jdbcTemplate.query(SqlQueries.GET_TRACK_LIST,param,BeanPropertyRowMapper.newInstance(Track.class));
        ResponseTrack responseTrack=new ResponseTrack();
        responseTrack.setScheduleId(scheduleId);
        for(Track t:track){
            t.setLink(url+"/"+t.getName());
        }
        responseTrack.setTrack(track);
        return responseTrack;
    }


    @Override
    public boolean CheckVehicle(String plateNumber, String vehicleKey) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("plateNumber",plateNumber);
        param.addValue("vehicleKey",vehicleKey);
        return jdbcTemplate.queryForObject(SqlQueries.CHECK_VEHICLE,param,Integer.class)>0;
    }



    @Override
    public Integer getVehicleId(String plateNumber, String vehicleKey) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("plateNumber",plateNumber);
        param.addValue("vehicleKey",vehicleKey);
        return jdbcTemplate.queryForObject(SqlQueries.GET_VEHICLE_ID,param,Integer.class);
    }

    @Override
    public List<Track> getTrackIdList(Integer scheduleId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("sId",scheduleId);
        return jdbcTemplate.query(SqlQueries.GET_UNICAL_TRACK_ID,param,BeanPropertyRowMapper.newInstance(Track.class));
    }

    @Override
    public boolean addVehicleTrack(TrackVehicle trackVehicle) {
        return jdbcTemplate.update(SqlQueries.ADD_VEHICLES_TRACK,new BeanPropertySqlParameterSource(trackVehicle))>0;
    }

    @Override
    public boolean updateVehicleTrack(Integer status, Integer trackId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("status",status);
        param.addValue("tId",trackId);

        return jdbcTemplate.update(SqlQueries.UPDATE_VEHICLES_TRACK,param)>0;
    }

    @Override
    public boolean deleteVehicleTrack(Integer vehicleId) {
        MapSqlParameterSource param=new MapSqlParameterSource();
        param.addValue("vId",vehicleId);
        return jdbcTemplate.update(SqlQueries.DELETE_VEHICLE_TRACK,param)>0;
    }
}
