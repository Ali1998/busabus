package service;


import dao.LogDao;
import domain.AndroidLog;
import domain.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LogService {

    @Autowired

    private LogDao logDao;


    public boolean addLog(Log log){return logDao.addLog(log); }

    public boolean addAndroidLog(AndroidLog log){return logDao.addAndroidLog(log);}


}
