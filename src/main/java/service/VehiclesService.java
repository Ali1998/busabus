package service;


import com.google.gson.JsonObject;
import dao.VehiclesDao;
import domain.ResponseTrack;
import domain.ResponseVehicles;
import domain.Track;
import domain.TrackVehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class VehiclesService {


    @Autowired
    private VehiclesDao vehiclesDao;


    public ResponseVehicles getScheduleData(String plateNumber, String vehicleKey){return vehiclesDao.getScheduleData(plateNumber,vehicleKey);}

    public Integer getScheduleId(String plateNumber, String vehicleKey){return vehiclesDao.getScheduleId(plateNumber,vehicleKey);}

    public ResponseTrack getTracks(Integer scheduleId){return vehiclesDao.getTracks(scheduleId);}

    public boolean CheckVehicle(String plateNumber, String vehicleKey){return vehiclesDao.CheckVehicle(plateNumber,vehicleKey);}

    public Integer getVehicleId(String plateNumber, String vehicleKey){return vehiclesDao.getVehicleId(plateNumber,vehicleKey);}

    public  List<Track> getTrackIdList(Integer scheduleId){return vehiclesDao.getTrackIdList(scheduleId);}

    public boolean addVehicleTrack(TrackVehicle trackVehicle){return vehiclesDao.addVehicleTrack(trackVehicle);}

    public boolean updateVehicleTrack(Integer status,Integer trackId){return vehiclesDao.updateVehicleTrack(status,trackId);}

    public boolean deleteVehicleTrack(Integer vehicleId){return vehiclesDao.deleteVehicleTrack(vehicleId);}


}
