package handler;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private SessionRegistry sessionRegistry;

    @Override
    public void onLogoutSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        try {
            User authUser = (User) authentication.getPrincipal();
//            userService.updateLogoutUser(authUser.getUsername());
            sessionRegistry.getSessionInformation(httpServletRequest.getRequestedSessionId()).expireNow();
        } catch (Exception e){

        }

        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + "/login");
    }



}
