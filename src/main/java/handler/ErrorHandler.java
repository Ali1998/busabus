package handler;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

    private static Logger logger = Logger.getLogger(ErrorHandler.class);

    @ExceptionHandler(AccessDeniedException.class)
    public String showAccessDenied(AccessDeniedException e){

        //System.out.println("Error occured: " + e.getMessage());
        logger.error("Error occured : ", e);

        return "error";
    }


    @ExceptionHandler(Exception.class)
    public void showError(Exception e){

        //System.out.println("Error occured: " + e.getMessage());
        logger.error("Error occured : ", e);
    }

}
