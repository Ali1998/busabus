package util;

import com.google.gson.Gson;
import domain.AndroidLog;
import domain.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.LogService;
import service.VehiclesService;

import java.util.Map;

@Service
public class Method {


    @Autowired
    public  VehiclesService vs;
    @Autowired
    public  LogService logService;

    public  boolean addLog(Map<String,Object>map,Map<String,Object>data,String apiName){

        String lat=null,lng=null,plateNumber=null,vehicleKey=null;
           Integer vId=null;
           Gson gson=new Gson();
           if(map.get("lat")!=null && map.get("lat").toString().trim().length()>0){
               lat=map.get("lat").toString();
           }
           if(map.get("lng")!=null && map.get("lng").toString().trim().length()>0){
               lng=map.get("lng").toString();
           }
           if(map.get("plateNumber")!=null && map.get("plateNumber").toString().trim().length()>0){
               plateNumber=map.get("plateNumber").toString();
           }
           if(map.get("vehicleKey")!=null && map.get("vehicleKey").toString().trim().length()>0){
               vehicleKey=map.get("vehicleKey").toString();
           }
           if(vs.CheckVehicle(plateNumber,vehicleKey)){
               vId=vs.getVehicleId(plateNumber,vehicleKey);
           }

           String requestBody=gson.toJson(map);
           String responseBody=gson.toJson(data);

           Log log=new Log();
           log.setApiName(apiName);
           log.setLat(lat);
           log.setLng(lng);
           log.setRequestBody(requestBody);
           log.setResponseBody(responseBody);
           log.setId(vId);
           boolean result=logService.addLog(log);

        return result;


    }


    public boolean addAndroidLog(Map<String,Object>map,String path){
        String plateNumber=null,vehicleKey=null;
        Integer vId=null;

        if(map.get("plateNumber")!=null && map.get("plateNumber").toString().trim().length()>0){
            plateNumber=map.get("plateNumber").toString();
        }
        if(map.get("vehicleKey")!=null && map.get("vehicleKey").toString().trim().length()>0){
            vehicleKey=map.get("vehicleKey").toString();
        }
        if(vs.CheckVehicle(plateNumber,vehicleKey)){
            vId=vs.getVehicleId(plateNumber,vehicleKey);
        }

        AndroidLog log=new AndroidLog();
        log.setVehicleId(vId);
        log.setPath(path);

        boolean result=logService.addAndroidLog(log);
        return result;
    }



}
