package domain;

import org.springframework.web.multipart.MultipartFile;

public class AndroidLog {

    private Integer vehicleId;

    private String path;

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}






