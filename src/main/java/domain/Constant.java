package domain;

public class Constant {

    public static final Integer INTERNAL_EXCEPTION=2000;
    public static final Integer ERROR=1001;
    public static final Integer SUCCESS=1000;
    public static final Integer NotInformation=1014;
    public static final Integer NothingChange=1003;
    public static final Integer NotFoundTrack=1007;
    public static final Integer WrongData=1009;
    public static final Integer NoValidation=1011;
    public static final Integer FileError=1005;
    public static final Integer WrongFormat=1016;





}
