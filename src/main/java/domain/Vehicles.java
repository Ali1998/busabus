package domain;

public class Vehicles {

    private String plateNumber;
    private String description;
    private String vehicleKey;
    private Integer updateLoop;
    private Integer scheduleId;


    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVehicleKey() {
        return vehicleKey;
    }

    public void setVehicleKey(String vehicleKey) {
        this.vehicleKey = vehicleKey;
    }

    public int getUpdateLoop() {
        return updateLoop;
    }

    public void setUpdateLoop(int updateLoop) {
        this.updateLoop = updateLoop;
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }
}
