package domain;

public class ResponseVehicles {

    private Integer updateLoop;
    private String description;
    private String plateNumber;
    private String vehicleKey;


    public void setUpdateLoop(Integer updateLoop) {
        this.updateLoop = updateLoop;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getVehicleKey() {
        return vehicleKey;
    }

    public void setVehicleKey(String vehicleKey) {
        this.vehicleKey = vehicleKey;
    }

    public int getUpdateLoop() {
        return updateLoop;
    }

    public void setUpdateLoop(int updateLoop) {
        this.updateLoop = updateLoop;
    }

}
