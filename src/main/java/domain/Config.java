package domain;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {


    private static final Logger logger = LogManager.getLogger(Config.class);

    public static final String CONFIG_FILE_PATH = "config.properties";

    public static final String TRACK_URL = get("track_url");

    public static final String LOGS_URL=get("logs_url");

    public static final String ANDROID_LOG_PATH=get("android_logs_path");





    private static Properties properties;

    public static String get(String keyword) {
        if (properties == null) {
            properties = getProperties();
            if (properties == null) {
                return null;
            }
        }
        return properties.getProperty(keyword);
    }

    private static Properties getProperties() {
        InputStream fis = null;
        try {

            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Properties props = new Properties();

            try {
                InputStream resourceStream = loader.getResourceAsStream(CONFIG_FILE_PATH);
                props.load(resourceStream);
            } catch (Exception e) {
                throw new IOException();
            }

            return props;

        } catch (IOException ex) {
            try {
                Properties property = new Properties();
                property.load(new FileInputStream("./config/" + CONFIG_FILE_PATH));

                return property;
            } catch (IOException exc) {
                logger.error("There is problem in reading Properties file ", ex);
                return null;
            }
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                logger.error("There is problem in closing FileInputStream", ex);
            }
        }
    }




}
