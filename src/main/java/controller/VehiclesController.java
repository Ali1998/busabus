package controller;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import service.VehiclesService;
import util.Method;

import java.io.File;
import java.io.IOException;
import java.util.*;

@RestController
public class VehiclesController {



    @Autowired
    private VehiclesService vehiclesService;

    @Autowired
    private Method method;



    private static Logger logger=Logger.getLogger(VehiclesController.class);

    @RequestMapping(value = "/vehicle/getVehicle", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getScheduleData(@RequestBody  Map<String,Object>map) {

        Map<String, Object> data = new HashMap<>();
        String apiName="/vehicle/getVehicle";


        try {

            String plateNumber=map.get("plateNumber").toString();
            String vehicleKey=map.get("vehicleKey").toString();

            if(vehiclesService.CheckVehicle(plateNumber,vehicleKey)){
                ResponseVehicles responseVehicles=vehiclesService.getScheduleData(plateNumber,vehicleKey);
                data.put("vehicle",responseVehicles);
                data.put("statusCode",Constant.SUCCESS);

            }else{
                data.put("statusCode",Constant.NoValidation);
                data.put("statusMessage","unauthorized");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("Error"+ex);
            ex.printStackTrace();

        }
        method.addLog(map,data,apiName);
        return data;

    }




    @RequestMapping(value = "/vehicle/getTracks", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getTracksData(@RequestBody  Map<String,Object>map) {

        Map<String, Object> data = new HashMap<>();
        ResponseTrack tracks;
        Integer sId,vId;
        String apiName="/vehicle/getTracks";

        try {

            String plateNumber=map.get("plateNumber").toString();
            String vehicleKey=map.get("vehicleKey").toString();

            if(vehiclesService.CheckVehicle(plateNumber,vehicleKey)){

                vId=vehiclesService.getVehicleId(plateNumber,vehicleKey);
                sId=vehiclesService.getScheduleId(plateNumber,vehicleKey);
                int scheduleId=Integer.parseInt(map.get("scheduleId").toString());


                if(scheduleId<-1){
                    data.put("statusCode",Constant.WrongData);
                    data.put("statusMessage","wrong scheduleId!");

                }else if(sId==null){
                    data.put("statusCode",Constant.NotInformation);
                    data.put("statusMessage","this vehicle not found schedule");

                }else if(sId==scheduleId){
                    data.put("statusCode",Constant.NothingChange);
                    data.put("statusMessage","Nothing Change");

                }else{

                    tracks=vehiclesService.getTracks(sId);
                    if(data==null){
                        data.put("statusCode",Constant.NotFoundTrack);
                        data.put("statusMessage","Not Found Tracks");

                    }else{
                        data.put("statusCode",Constant.SUCCESS);
                        data.put("tracks",tracks);
                        List<Track>list=vehiclesService.getTrackIdList(sId);
                        vehiclesService.deleteVehicleTrack(vId);
                        for(Track t:list){

                            TrackVehicle trackVehicle=new TrackVehicle();
                            trackVehicle.setsId(sId);
                            trackVehicle.settId(t.getId());
                            trackVehicle.setvId(vId);
                            vehiclesService.addVehicleTrack(trackVehicle);
                        }
                    }
                }
            }else{
                data.put("statusCode",Constant.NoValidation);
                data.put("statusMessage","unauthorized");
            }



        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("Error"+ex);

        }

        method.addLog(map,data,apiName);

        return data;

    }






    @RequestMapping(value = "/vehicle/updateTrack", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> changeStatusTrack(@RequestBody  Map<String,Object>map) {

        Map<String, Object> data = new HashMap<>();
        String apiName="/vehicle/updateTrack";

        try {

            String plateNumber=map.get("plateNumber").toString();
            String vehicleKey=map.get("vehicleKey").toString();
            Integer trackId=Integer.parseInt(map.get("trackId").toString());
            Integer status=Integer.parseInt(map.get("status").toString());

            if(vehiclesService.CheckVehicle(plateNumber,vehicleKey)){
                if(vehiclesService.updateVehicleTrack(status,trackId)){
                    data.put("statusCode", Constant.SUCCESS);
                    data.put("statusMessage","Track status is updated!");
                }

            }else{
                data.put("statusCode",Constant.NoValidation);
                data.put("statusMessage","unauthorized");
            }


        } catch (Exception ex) {
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("statusMessage", "System Error");
            logger.info("Error"+ex);

        }

        method.addLog(map,data,apiName);

        return data;

    }



    @RequestMapping(value = "/vehicle/uploadLog", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> uploadLog(@RequestParam(value = "file") MultipartFile file,@RequestParam(value="data") String json) {


        Map<String, Object> data = new HashMap<>();
        String apiName="/vehicle/uploadLog";
        Map<String,Object>map=new HashMap<>();


        try{

            map= new ObjectMapper().readValue(json, HashMap.class);

            if (!file.isEmpty()) {

                String fileFormat = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));

                if(fileFormat.equalsIgnoreCase(".txt")) {

                    try {

                        String realPath = Config.LOGS_URL;
                        String androidLogPath=Config.ANDROID_LOG_PATH;

                    if (!new File(realPath).exists()) {
                        new File(realPath).mkdir();
                    }

                        String filePath = realPath +file.getOriginalFilename();
                        File dest = new File(filePath);
                        file.transferTo(dest);
                        data.put("statusCode",Constant.SUCCESS);
                        data.put("statusMessage","File has been successfully added!");
                        String path=androidLogPath+file.getOriginalFilename();
                        method.addAndroidLog(map,path);



                    } catch (IOException e) {
                        data.put("statusCode", Constant.FileError);
                        data.put("statusMessage", "fileError");
                        e.printStackTrace();
                    }
                } else {
                    data.put("statusCode", Constant.WrongFormat);
                    data.put("state", "wrongFormat");
                }
            }



        }catch(Exception ex){
            data.put("statusCode", Constant.INTERNAL_EXCEPTION);
            data.put("state", "System error");
            logger.info("error:"+ex);
        }

        method.addLog(map,data,apiName);



        return data;
    }








}
